public class Vehiculo
{
	protected int CostoAlquilerDia;
	protected String placa;
	
	public Vehiculo(String placa)
	{
		this.CostoAlquilerDia = 150000;
		this.placa = placa;
	}
	
	public String getPlaca()
	{
		return this.placa;
	}
	public int CalcularAlquiler(int dias)
	{
		return (CostoAlquilerDia*dias);
	}
}
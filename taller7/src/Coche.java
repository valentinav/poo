public class Coche extends Vehiculo
{
	private int CostoDiaCoche;
	
	public Coche (String placa)
	{
		super(placa);
		CostoDiaCoche=0;
	}
	public int CalcularAlquiler(int dias)
	{
		CostoDiaCoche = (3000*dias);
		return (super.CalcularAlquiler(dias)+CostoDiaCoche);
	}
}
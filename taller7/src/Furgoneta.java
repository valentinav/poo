public class Furgoneta extends Vehiculo
{	
	private int PMA;
	
	public Furgoneta(String placa, int PMA)
	{
		super(placa);
		this.PMA = PMA;
	}
	public int CalcularAlquiler(int dias)
	{
		return (super.CalcularAlquiler(dias)+(60000*PMA));
	}
}
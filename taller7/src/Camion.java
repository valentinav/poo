public class Camion extends Vehiculo
{
	private int FIJO;
	
	public Camion(String placa)
	{
		super(placa);
		FIJO = 120000;
	}
	public int CalcularAlquiler(int dias)
	{
		return (super.CalcularAlquiler(dias)+FIJO);
	}
}
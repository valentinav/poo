public class Cliente
{
	public static void main(String [] args)
	{
		ListadoVehiculos lv = new ListadoVehiculos();
		
		lv.agregarCoche("QEQ015"); 
		lv.agregarMicrobus("BBC033");
		lv.agregarFurgoneta("SRC462", 50);
		lv.agregarCamion ("PPF001");
		
		Vehiculo varLv1 = lv.buscarVehiculo("BBC033");
		if (varLv1 == null)
		{
			System.out.println ("El vehiculo no existe");
		}
		else
		{
			System.out.println ("Precio de alquiler del vehiculo buscado es: " + varLv1.CalcularAlquiler(5));
		}
	
		lv.darTodosLosPreciosDeAlquiler(5);
	}
}

public class Microbus extends Vehiculo
{
	private int CostoAdicional;
	
	public Microbus(String placa)
	{
		super(placa);
		CostoAdicional = 6000;
	}
	public int CalcularAlquiler(int dias)
	{
		return ((super.CalcularAlquiler(dias)+(3000*dias))+CostoAdicional);
	}
}
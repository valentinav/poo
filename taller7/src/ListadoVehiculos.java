import java.util.ArrayList;

public class ListadoVehiculos
{	
	private ArrayList <Vehiculo> vehiculos;
	
	public ListadoVehiculos()
	{
		vehiculos = new ArrayList <Vehiculo>();
	}
	public void agregarCoche(String placa)
	{
		Coche c = new Coche(placa);
		vehiculos.add(c);
	}
	public void agregarMicrobus(String placa)
	{
		Microbus MB = new Microbus(placa);
		vehiculos.add(MB);
	}
	public void agregarFurgoneta(String placa, int PMA)
	{
		Furgoneta f = new Furgoneta(placa, PMA);
		vehiculos.add(f);
	}
	public void agregarCamion(String placa)
	{
		Camion Ca = new Camion(placa);
		vehiculos.add(Ca);
	}
	public Vehiculo buscarVehiculo(String placa)
	{
		Vehiculo objVehiculo= null;
		for (int i=0;i<vehiculos.size();i++)
		{
			if((vehiculos.get(i).getPlaca())== placa)
			{
				objVehiculo = vehiculos.get(i);
				break;
			}
		}
		return objVehiculo;
	}
	
	public void darTodosLosPreciosDeAlquiler (int dias)
	{
		for (int i=0;i<vehiculos.size();i++)
		{
			System.out.println("precios de alquiler: "+ vehiculos.get(i).CalcularAlquiler(dias));
		}
	}
	
}
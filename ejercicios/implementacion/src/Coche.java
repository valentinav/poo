public class Coche
{
	
	private int atrNumeroGaraje;
	private String atrMarca;
	private String atrPlaca;
	private int atrPrecio;
	private boolean atrDisponibilidad;
	
	
	public Coche(int refGaraje, String refMarca, String refPlaca, int Dinero)
	{
		atrNumeroGaraje = refGaraje;
		atrMarca = refMarca;
		atrPlaca = refPlaca;
		atrPrecio = Dinero;
		atrDisponibilidad = true;
	}
	
	public int GetGaraje ()
	{
		return atrNumeroGaraje;
	}
	
	public String GetMarca ()
	{
		return atrMarca;
	}
	
	public String GetPlaca ()
	{
		return atrPlaca;
	}
	
	public int GetPrecio ()
	{
		return atrPrecio;
	}
	
	
}
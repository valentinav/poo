public class Revistas extends LibrosClasicos
{
	private int Volumen;
	private int NumeroSalida;
	private int MesSalida;
	
	public Revistas (int ID, String Titulo, String Autores, String Editorial, int AnioPublicacion, int Volumen, int NumeroSalida, int MesSalida)
	{
		super (ID, Titulo, Autores, Editorial, AnioPublicacion);
		this.Volumen = Volumen;
		this.NumeroSalida = NumeroSalida;
		this.MesSalida = MesSalida;
	}

}
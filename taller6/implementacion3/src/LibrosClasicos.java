public class LibrosClasicos 
{
	protected String Titulo;
	protected String Autores;
	protected String Editorial;
	protected int AnioPublicacion;
	protected int ID;
	
	public LibrosClasicos (int ID, String Titulo, String Autores, String Editorial, int AnioPublicacion)
	{
		this.ID = ID;
		this.Titulo = Titulo;
		this.Autores = Autores;
		this. Editorial = Editorial;
		this.AnioPublicacion = AnioPublicacion;
	}
	
	public int getID()
	{
		return ID;
	}
	
}
public class Biblioteca
{
	private LibrosClasicos Libro[];
	private ConsultasSala  Acceso1[];
	private PrestamoTemporal Acceso2[];
	private Object Apuntador;
	
	public Biblioteca()
	{
		Libro = new LibrosClasicos[4];
		Libro [0] = new LibrosClasicos(1,"Juego de Tronos", "George R.R. Martin", "Norma", 2000);
		Libro [1] = new Revistas(2,"Muy Interesante", "Pepito", "Española", 2010, 6, 12, 5);
		Libro [2] = new FormatoCD(3,"Mil Ciudades", "Andres Cepeda", "Universal", 2015, "Mp3", "Copyright");
		Libro [3] = new Microfilms(4,"La Mariposa", "Juan", "acer", 1997, 3, 7, 10);
		
		Acceso1 = new ConsultasSala[2];
		Acceso1 [0] = new ConsultasSala("22/10/2015", 8888);
	
		Acceso2 = new PrestamoTemporal [2];
		Acceso2 [0] = new PrestamoTemporal("7/05/2015", 666);
	}
	public int BuscarDocumento(int ID)
	{
		int i;
		for (i = 0; i<4; i++)
		{
			if (Libro[i].getID()==ID)
			{
				Apuntador = Libro[i];
			}
		}
		return i;
	}
	public void Prestar(int ID ,String TipoAcceso)
	{
		int i = BuscarDocumento(ID);
		if (TipoAcceso == "consulta")
		{
			if (Acceso1[i].DocumentoDisponible() == true)
			{
				Acceso1[i].RecogerDocumento(Apuntador);
				System.out.println ("Documeto consultado con exito");
			}
			else
			{
				System.out.println ("el documento no se puede consultar");
			}
		}
		if (TipoAcceso == "prestamo")
		{
			if ((ID != 4)&(Acceso2[i].DocumentoDisponible() == true))
			{
				Acceso2[i].RecogerDocumento(Apuntador);
				Acceso2[i].ReservarDocumento();
				System.out.println ("Document prestado con exito");
			}
			else
			{
				System.out.println ("El documente no se puede prestar");
			}				
		}
	}
}
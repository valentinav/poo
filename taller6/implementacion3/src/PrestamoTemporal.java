public class PrestamoTemporal extends ConsultasSala
{
	public PrestamoTemporal(String fecha, int DNI)
	{
		super(fecha, DNI);
	}
	
	public void ReservarDocumento()
	{
		Disponible = false;
	}
	public boolean DocumentoDisponible()
	{
		return Disponible;
	}
}
public class ConsultasSala
{
	protected String fecha;
	protected int DNI;
	protected Object Asignacion;
	protected boolean Disponible = true;
	
	public ConsultasSala(String fecha, int DNI)
	{
		this.fecha = fecha;
		this.DNI = DNI;
	}
	
	public void RecogerDocumento(Object apuntador)
	{
		this.Asignacion = apuntador;
		Disponible = false;
	}
	public void DevolverDocumento()
	{
		this.Asignacion = null;
		Disponible = true;
	}
	public boolean DocumentoDisponible()
	{
		return Disponible;
	}
	
	
}
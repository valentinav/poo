public class FormatoCD extends LibrosClasicos
{
	private String FormatoCD;
	private String TipoLicencia;
	
	public FormatoCD(int ID, String Titulo, String Autores, String Editorial, int AnioPublicacion, String FormatoCD, String TipoLicencia)
	{
		super (ID, Titulo, Autores, Editorial, AnioPublicacion);
		this.FormatoCD = FormatoCD;
		this.TipoLicencia = TipoLicencia;
	}
}
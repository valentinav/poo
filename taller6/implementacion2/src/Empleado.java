public class Empleado
{
	protected String Nombre;
	protected int edad;
	protected int ID;
	protected int Saldo;
	
	public Empleado (String Nombre, int edad, int ID, int Saldo)
	{
		this.Nombre = Nombre;
		this.edad = edad;
		this.ID = ID;
		this.Saldo = Saldo;
	}
	
	public int generarSueldo()
	{
		return this.Saldo;
	}
	public String getNombre()
	{
		return this.Nombre;
	}
	public int getEdad()
	{
		return this.edad;
	}
	public int getID()
	{
		return this.ID;
	}
}
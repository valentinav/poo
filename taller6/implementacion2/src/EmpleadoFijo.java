public class EmpleadoFijo extends Empleado
{
	private int AnioInicio;
	private int AnioActual;
	private int ComplementoAnual;
	
	public EmpleadoFijo (String Nombre, int edad, int ID, int Saldo, int AnioInicio, int AnioActual, int ComplementoAnual)
	{
		super(Nombre, edad, ID, Saldo);
		this.AnioInicio = AnioInicio;
		this.AnioActual = AnioActual;
		this.ComplementoAnual = ComplementoAnual;
	}
	public int generarSueldo ()
	{
		return super.generarSueldo()+(ComplementoAnual*TotaldeAnios()); 
	}
	public int TotaldeAnios()
	{
		return (AnioActual-AnioInicio);
	}
}
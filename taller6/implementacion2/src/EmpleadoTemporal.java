public class EmpleadoTemporal extends Empleado
{
	private int FechaInicio;
	private int FechaDespedida;
	
	public EmpleadoTemporal (String Nombre, int edad, int ID, int Saldo, int FechaInicio,  int FechaDespedida)
	{
		super (Nombre, edad, ID, Saldo);
		this.FechaInicio = FechaInicio;
		this.FechaDespedida = FechaDespedida;
	}
	public int generarSueldo ()
	{
		return this.Saldo;
	}
}
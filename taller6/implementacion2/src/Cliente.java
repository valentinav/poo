public class Cliente
{
	public static void main (String[] args)
	{
		Empleado Empleados[] = new Empleado [3];
		Empleados [0] = new EmpleadoTemporal ("Juan", 27, 236, 56000, 6, 20); 
		Empleados [1] = new EmpleadoPorHoras ("Miguel", 33, 987, 30000, 45);
		Empleados [2] = new EmpleadoFijo ("Andres", 20, 655, 88000, 1997, 2015, 10000);
		
		for (int i=0; i<3; i++)
		{
			System.out.println ("Nombre: "+ Empleados[i].getNombre() +"\nedad: "+ Empleados[i].getEdad() +", ID: "+ Empleados[i].getID() +", Saldo: "+ Empleados[i].generarSueldo());
		}
 	}
}
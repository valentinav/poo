public class EmpleadoPorHoras extends Empleado
{
	private int PrecioHora; //Precio fijo
	private int HorasPorMes;
	
	public EmpleadoPorHoras (String Nombre, int edad, int ID,int Saldo, int HorasPorMes)
	{
		super (Nombre, edad, ID, Saldo);
		this.PrecioHora = 0;
		this.HorasPorMes = HorasPorMes;
	}
	public int generarSueldo ()
	{
		return super.generarSueldo()*(HorasPorMes);
	}
}
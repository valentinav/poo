public class AlquilerErroneo extends Exception
{
	public AlquilerErroneo()
	{
		super("el valor del alquiler no debe exceder de un millon de pesos");
	}
}
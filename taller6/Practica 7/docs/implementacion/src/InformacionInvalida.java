public class InformacionInvalida extends Exception
{
	public InformacionInvalida()
	{
		super("la eslora no puede exceder de 30 metros");
	}
}
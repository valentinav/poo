public class Deportivo extends Barco{
	private int potencia;
	
	public Deportivo(String matricula, int eslora, int anio, int potencia)throws InformacionInvalida
	{
		super(matricula,eslora,anio);
		this.potencia = potencia;
	}
	public int alquiler(int d) throws AlquilerErroneo
	{
		return super.alquiler(d)+(potencia*2000);
	}
}
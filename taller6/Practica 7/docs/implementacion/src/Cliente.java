public class Cliente{
	public static void main(String[] args){
			
		Barco b[] = new Barco[3];
		try
		{
			b[0] = new Velero("MA01",30,1999,12);
			b[1] = new Deportivo("MA02",10,1978,34);
			b[2] = new Deportivo("MA03",88,2009,50);
		}
		catch (InformacionInvalida E)
		{
			System.err.println(E.getMessage());
			E.printStackTrace();
		}
		
		for (int i = 0; i<3; i++){
			try
			{
				System.out.println("Matricula: "+b[i].getMatricula()+" Alquiler: "+b[i].alquiler(4000));
			}
			catch (AlquilerErroneo Al)
			{
				System.err.println(Al.getMessage());
				Al.printStackTrace();
			}
		}
		
	}
	
}
public class Barco {
	protected int valorFijo;
	protected String matricula;
	protected int eslora;
	protected int anio;
	
	public Barco(String matricula, int eslora, int anio) throws InformacionInvalida
	{
		this.matricula = matricula;
		if (eslora > 30) throw (new InformacionInvalida());
		this.eslora = eslora;
		this.anio = anio;		
	}
	
	public String getMatricula(){
		return matricula;
	}
	public void setMatricula(String ma){
		ma = matricula;		
	}
	public int getEslora(){
		return eslora;
	}
	public void setEslora(int esl){
		esl = eslora;
	}
	public int getAnio(){
		return anio;
	}
	public void setAnio(int an){
		an = anio;
	}
	public int alquiler(int d) throws AlquilerErroneo
	{
		valorFijo = (d*6000)+(eslora*1000);
		return valorFijo;
	}
	
}
public class Velero extends Barco {
	private int mastiles;
	
	public Velero (String matricula, int eslora, int anio, int mastiles) throws InformacionInvalida
	{
		super (matricula,eslora,anio);
		this.mastiles = mastiles;
	}
	public int getMastiles(){
		return mastiles;
	}
	public void setMastiles(int mastiles){
		this.mastiles = mastiles;
	}
	public int alquiler(int d) throws AlquilerErroneo
	{
		valorFijo = super.alquiler(d)+(mastiles*10000);
		if (valorFijo > 1000000) throw (new AlquilerErroneo());
		
		return valorFijo;
	}
	
	
}
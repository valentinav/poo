public class Velero extends Barco
{
	private int Mastiles;
	
	public Velero(String nombre, int DNI, String Matricula, int eslora, int anio, int Mastiles)
	{
		super (nombre,DNI,Matricula, eslora, anio);
		this.Mastiles = Mastiles;
	}
	public int getMastiles()
	{
		return Mastiles;
	}
	
	public int Alquiler ()
	{
		return super.Alquiler()+(Mastiles);
	}
}
public class Barco
{
	private int VALORFIJO;
	protected String nombre;
	protected int DNI;
	protected String Matricula;
	protected int eslora;
	protected int anio;
	protected int cantidaddeDias;
	
	public Barco(String nombre, int DNI, String Matricula, int eslora, int anio)
	{
		this.nombre = nombre;
		this.DNI=DNI;
		this.Matricula = Matricula;
		this.eslora = eslora;
		this.anio = anio;
	}
	public String getMatricula ()
	{
		return Matricula;
	}
	
	public int getEslora ()
	{
		return eslora;
	}
	
	public int getAnio()
	{
		return anio;
	}
	public String getNombre()
	{
			return nombre;
	}
	public void DiasdeAlquiler(int diaInicial, int diaFinal)
	{
		cantidaddeDias = (diaFinal-diaInicial);
	}
	public int Alquiler ()
	{
		VALORFIJO = (((cantidaddeDias)*(eslora*10))*6000);
		return VALORFIJO;
	}
}
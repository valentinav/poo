public class Deportivo extends Barco
{
	private int Potencia;
	
	public Deportivo(String nombre, int DNI, String Matricula, int eslora, int anio, int Potencia)
	{
		super(nombre,DNI,Matricula, eslora, anio);
		this.Potencia = Potencia;
	}
	public int Alquiler ()
	{
		return super.Alquiler()+(Potencia);
	}
}
public class Yate extends Barco
{
	private int Potencia;
	private int numCamarotes;
	
	public Yate(String nombre, int DNI, String Matricula, int eslora, int anio, int Potencia, int numCamarotes)
	{
		super(nombre, DNI, Matricula, eslora, anio); 
		this.Potencia = Potencia;
		this.numCamarotes = numCamarotes;
	}
	public int Alquiler ()
	{
		return super.Alquiler()+(Potencia+numCamarotes);
	}
}

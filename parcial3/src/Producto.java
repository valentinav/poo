public class Producto
{
	protected String Nombre;
	protected int valor;
	
	public Producto (String Nombre, int v) throws IlegalValorException
	{
		if ((v<5000)||(v>100000)) throw (new IlegalValorException());
		
		this.Nombre = Nombre;
		this.valor = v;
	}
	
	public int getValor()
	{
		return valor;
	}
	public String getNombre()
	{
		return Nombre;
	}
	
	public String mostrar()
	{
		return "Nombre : " + getNombre() + "valor: " +getValor();
	}
}
public class Platillo extends Producto
{
	private String Nombre2;
	
	public Platillo(String Nombre, int valor, String Nombre2) throws IlegalValorException
	{
		super(Nombre, valor);
		this.Nombre2 = Nombre2;
	}
	public String getNombre2()
	{
		return Nombre2;
	}
	public String mostrar()
	{
		return "Nombre : " + getNombre() + getNombre2() + "valor: " +getValor();
	}
}
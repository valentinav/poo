public class ClienteTest
{
	public static void main(String[] args)
	{
		Restaurante res = new Restaurante();
		
		try
		{
			Bebida b1 = new Bebida ("vino blanco casa rosada", 60000);
			Bebida b2 = new Bebida ("limonada natural", 20000);
			Platillo p1 = new Platillo ("lomo de cerdo", 23000, "lomus");
			Platillo p2 = new Platillo ("pescado frito", 15000, "fish");
			//Platillo p3 = new Platillo ("Otro", 150000, "Otro en frances");
			//Platillo p4 = new Platillo ("Otro2", -500, "otro en frances2");
			
			res.agregarProducto(b1);
			res.agregarProducto(b2);
			res.agregarProducto(p1);
			res.agregarProducto(p2);
		
			System.out.println ("MENU RESTAURANTE\n");
			res.mostrarProductos();
			
		}
		catch (IlegalValorException E)
		{
			System.err.println(E.getMessage());
			E.printStackTrace();
		}
	}
}
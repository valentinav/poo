import java.util.ArrayList;

public class Restaurante
{
	private ArrayList <Producto> menu;
	
	public Restaurante()
	{
		menu = new ArrayList <Producto>();
	}
	public void agregarProducto(Producto p)
	{
		menu.add(p);									
	}
	public void mostrarProductos()
	{
		for(int i=0;i<menu.size();i++)
		{
			System.out.println(menu.get(i).mostrar());
		}
	}
}
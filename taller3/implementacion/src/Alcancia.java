public class Alcancia
{

	private Moneda m20;
	private Moneda m50;
	private Moneda m100;
	private Moneda m200;
	private Moneda m500;
	
	public Alcancia ()
	{
		m20 = new Moneda (20);
		m50 = new Moneda (50);
		m100 = new Moneda (100);
		m200 = new Moneda (200);
		m500 = new Moneda (500);
	}
	
	public void agregarMoneda (int dm)
	{
		switch (dm)
		{
			case 20:
				m20.acumularDenominacion();
				break;
			case 50:
				m50.acumularDenominacion ();
				break;
			case 100:
				m100.acumularDenominacion ();
				break;
			case 200:
				m200.acumularDenominacion();
				break;
			case 500:
				m500.acumularDenominacion();
				break;
		}
	}
	
	public int cantidadDenom(int dm)
	{
		int c = 0;
		if (m20.getDenominacion() == dm)
		{
			c = m20.getCantidad();
		}
		if (m50.getDenominacion() == dm)
		{
			c = m50.getCantidad();
		}
		if (m100.getDenominacion() == dm)
		{
			c = m100.getCantidad();
		}
		if (m200.getDenominacion() == dm)
		{
			c = m200.getCantidad();
		}
		if (m500.getDenominacion() == dm)
		{
			c = m500.getCantidad();
		}
		return c;
	}
	
	public int valorAhorrado()
	{
		int v = 0;
		v = m20.darCantidadDinero() + m50.darCantidadDinero() + m100.darCantidadDinero() + m200.darCantidadDinero() + m500.darCantidadDinero();
		return v;
	}
}
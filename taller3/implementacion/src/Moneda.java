public class Moneda
{
	private int denominacion;
	private int cantidad;

	public Moneda(int dm)
	{
		denominacion = dm;
		cantidad = 0;
	}
	
	public int darCantidadDinero()
	{
		int c;
		c = denominacion*cantidad;
		return c;
	}
	
	public int getCantidad ()
	{
		return cantidad;
	}
	
	public int getDenominacion ()
	{
		return denominacion;
	}
	
	public void acumularDenominacion ()
	{
		cantidad ++;
	}
}
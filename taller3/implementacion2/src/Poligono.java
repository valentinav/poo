public class Poligono 
{
	private Punto p1;
	private Punto p2;
	private Punto p3;
	private Punto p4;
	private Punto p5;
	private int radio;
	private String color;
	private String llenado;
	private int Lados;
	
	public Poligono ()
	{
		radio = 0;
		color = " ";
		llenado = " ";
	}
	
	public void CrearPoligono (int NLados)
	{	
		Lados = NLados;
		switch (Lados) 
		{
			case 1:
			p1 = new Punto (3, 3);
			radio = 5;
			color = "amarillo";
			llenado = "si";
			break;
			
			
			case 3:
			p1 = new Punto (1, 6);
			p2 = new Punto (1, 1);
			p3 = new Punto (5, 1);
			color = "rojo";
			llenado = "no";
			break;
			
			
			case 4:
			p1 = new Punto (3, 8);
			p2 = new Punto (3, 1);
			p3 = new Punto (10, 8);
			p4 = new Punto (10, 1);
			color = "verde";
			llenado = "no";
			break;
			
			case 5:
			p1 = new Punto (3, 9);
			p2 = new Punto (2, 4);
			p3 = new Punto (2, 2);
			p4 = new Punto (7, 2);
			p5 = new Punto (7, 4);
			color = "azul";
			llenado = "si";
			break;
		}
	}
	
	public void imprimirPoligono ()
	{
		switch (Lados) 
		{
			case 1:
			System.out.println ("el punto 1 es:(" + p1.getEjex()+", "+ p1.getEjey()+")");
			System.out.println ("el radio es: " + radio);
			System.out.println ("el color es: " + color);
			System.out.println ("esta lleno? " + llenado);
			break;
			
			
			case 3:
			System.out.println ("el punto 1 es:(" + p1.getEjex()+", "+ p1.getEjey()+")");
			System.out.println ("el punto 2 es:(" + p2.getEjex()+", "+ p2.getEjey()+")");
			System.out.println ("el punto 3 es:(" + p3.getEjex()+", "+ p3.getEjey()+")");
			System.out.println ("el color es: " + color);
			System.out.println ("esta lleno? " + llenado);
			break;
			
			
			case 4:
			System.out.println ("el punto 1 es:(" + p1.getEjex()+", "+ p1.getEjey()+")");
			System.out.println ("el punto 2 es:(" + p2.getEjex()+", "+ p2.getEjey()+")");
			System.out.println ("el punto 3 es:(" + p3.getEjex()+", "+ p3.getEjey()+")");
			System.out.println ("el punto 4 es:(" + p4.getEjex()+", "+ p4.getEjey()+")");
			System.out.println ("el color es: " + color);
			System.out.println ("esta lleno? " + llenado);
			break;
			
			case 5:
			System.out.println ("el punto 1 es:(" + p1.getEjex()+", "+ p1.getEjey()+")");
			System.out.println ("el punto 2 es:(" + p2.getEjex()+", "+ p2.getEjey()+")");
			System.out.println ("el punto 3 es:(" + p3.getEjex()+", "+ p3.getEjey()+")");
			System.out.println ("el punto 4 es:(" + p4.getEjex()+", "+ p4.getEjey()+")");
			System.out.println ("el punto 5 es:(" + p5.getEjex()+", "+ p5.getEjey()+")");
			
			
			System.out.println ("el color es: " + color);
			System.out.println ("esta lleno? " + llenado);
			break;
		}
	}
}                                                                                                                      
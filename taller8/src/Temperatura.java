public class Temperatura
{
	private double kel;
	private double cen;
	private double far;
	private char t;
	
	public Temperatura(char t, float u)throws TemperaturaNoValidaException
	{
		this.t = t;
		if (t=='k')
		{
			if (u<0) throw (new TemperaturaNoValidaException());
			kel = u;
			cen = 0;
			far = 0;
		}
		if(t=='c'){cen=u;kel=0;far=0;}
		if(t=='f'){far=u;cen=0;kel=0;}	
	}
	public void convertirTem()throws TemperaturaNoValidaException
	{
		switch (t)
		{
			case 'k': {
				cen= kel-273.14;
				far =1.8*cen+23;
				break;
			}
			case 'c':{
				far= 1.8*cen+23;
				kel=cen+273.14;
				break;
			}
			case 'f':{
				cen=(far-32)/1.8;
				kel=cen+273.14;
				break;
			}
		}
		if (kel<0)throw (new TemperaturaNoValidaException());
	}
	public char getTipo()
	{
	return t;
	}
	public double getCen()
	{
		return cen;
	}
	public double getFar()
	{
		return far;
	}
	public double getKel()
	{
		return kel;
	}
}
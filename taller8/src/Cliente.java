public class Cliente
{
	public static void main (String[] args)
	{
		Temperatura t;
		try 
		{
			t= new Temperatura('k',-3);
			t.convertirTem();
			
			switch (t.getTipo())
			{
				case 'k':{
					System.out.Println(t.getKel()+"Grados Kelvin son: "+t.getCen()+" grados centigrados; "+t.getFar()+" grados fahrenheit");
					break;
				}
				case 'c':{
					System.out.println (t.getCen()+"grados centrigados son: "+t.getKel()+" grados kelvin;"+t.getFar()+" grados fahrenheit");
					break;
				}
				case 'f':{
					System.out.println(t.getFar()+"grados fahrenheit son: "+t.getCen()+" grados centigrados;"+t.getKel()+" grados kelvin");
					break;
				}
			}
		}
		catch (TemperaturaNoValidaException e)
		{
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
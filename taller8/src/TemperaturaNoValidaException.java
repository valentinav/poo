public class TemperaturaNoValidaException extends Exception
{
	public TemperaturaNoValidaException()
	{
		super("Los grados Kelvin no pueden ser menores que cero");
	}
}
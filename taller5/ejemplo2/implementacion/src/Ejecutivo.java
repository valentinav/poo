public class Ejecutivo extends Empleado
{
	private int presupuesto;
	
	public Ejecutivo (String cedula, String nombre, int sueldo, int presupuesto)
	{
		super(cedula, nombre, sueldo);
		this.presupuesto = presupuesto;
	}
	
	public Ejecutivo ()
	{
		super ();
		this.presupuesto = 0;
	}
	
	public int getPresupuesto ()
	{
		return presupuesto;
	}
	
	public void setPresupuesto (int presupuesto)
	{
		this.presupuesto = presupuesto;
	}
	
	public String toString ()
	{
		return "\n Cedula: " + getCedula() + "\n Nombre: " + getNombre() + "\n Sueldo: " + getSueldo() + "\n Presupuesto: " + presupuesto;
	}
}
public class Empleado
{
	protected String cedula;
	protected String nombre;
	protected int sueldo;
	
	public Empleado (String cedula, String nombre, int sueldo)
	{
		this.cedula = cedula;
		this.nombre = nombre;
		this.sueldo = sueldo;
	}
	
	public Empleado ()
	{
		this.cedula = "";
		this.nombre = "";
		this.sueldo = 0;
	}
	
	public void aumentarSueldo (int porcentaje)
	{
		sueldo += (int) (sueldo*porcentaje/100);
	}
	
	public String toString()
	{
		return "\n Cedula: " + cedula + "\n Nombre: " + nombre + "\n Sueldo: " + sueldo;
	}
	
	public String getCedula ()
	{
		return cedula;
	}
	
	public void setCedula (String cedula)
	{
		this.cedula = cedula;
	}
	
	public String getNombre ()
	{
		return nombre;
	}
	
	public void setNombre (String nombre)
	{
		this.nombre = nombre;
	}
	
	public int getSueldo ()
	{
		return sueldo;
	}
	
	public void setSueldo (int sueldo)
	{
		this.sueldo = sueldo;
	}
}
public class Cliente 
{
	public static void main (String[] args)
	{
		Empleado emp1 = new Empleado ("56749123", "Mario Sanchez", 2500000);
		emp1.aumentarSueldo(20);
		System.out.println ("Datos Empleado 1: " + emp1.toString());
		
		Ejecutivo ejec1 = new Ejecutivo ("98476523", "Fernando Yepes", 6000000, 1000000);
		ejec1.aumentarSueldo(10);
		System.out.println ("\n Datos Ejecutivo 1: " +ejec1);
	}
}
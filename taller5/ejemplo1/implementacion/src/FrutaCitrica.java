public class FrutaCitrica extends Fruta
{
	private int ph;
	
	public FrutaCitrica (int peso, int calorias, int ph)
	{
		super(peso,calorias);
		if (ph < 7)
		{
			this.ph = ph;
		}
		else 
		{
			this.ph = 7;
		}
	}
	
	public int calcularTotalCalorias ()
	{
		int total = super.calcularTotalCalorias()-(7-ph)*this.getCaloriasxgramo ();
		return total;
	}
	
}
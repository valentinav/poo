public class Zona
{
	private String Nombre;
	private Entrada entradas[];
	private int numZona;
	private int NumeroEntradas;
	private int IDEntradas;
	private int contEntradas;
	
	
	public Zona(int numZona ,String tzona, int numAsientos, int nOID)
	{
		this.numZona = numZona;
		Nombre = tzona;
		NumeroEntradas = numAsientos;
		contEntradas = 0;
		entradas = new Entrada[numAsientos];
		IDEntradas = nOID;
	}
	public boolean crearEntrada(Persona comprador,int z)
	{
		boolean entradasDisponibles= false;
		if(contEntradas <= NumeroEntradas)
		{
			if (comprador.getDocumento() == "estudiante")
			{
				entradas[contEntradas] = new EntradaReducida(IDEntradas, comprador, z);
				entradas[contEntradas].obtenerValorEntrada();
				contEntradas++;
				IDEntradas++;
				entradasDisponibles = true;
			}	
			if (comprador.getDocumento() == "pensionado")
			{
				entradas[contEntradas] = new EntradaReducida(IDEntradas, comprador, z);
				entradas[contEntradas].obtenerValorEntrada();
				contEntradas++;
				IDEntradas++;
				entradasDisponibles = true;
			}
			if (comprador.getDocumento() == "abonado")
			{
				entradas[contEntradas] = new EntradaAbonada(IDEntradas, comprador, z);
				entradas[contEntradas].obtenerValorEntrada();
				contEntradas++;
				IDEntradas++ ;
				entradasDisponibles = true;
			}
			if (comprador.getDocumento() == "normal")
			{
				entradas[contEntradas] = new EntradaNormal(IDEntradas, comprador, z);
				entradas[contEntradas].obtenerValorEntrada();
				contEntradas++;
				IDEntradas++;
				entradasDisponibles = true;
			}
		}
		
		return entradasDisponibles;
	}
	public boolean imprimirEntrada()
	{	
		int i;
		
		if (contEntradas > 0 )
		{
			for (i = 0; i < contEntradas; i++)
			{
				
				System.out.println("Identificador entrada:"+entradas[i].getID()+ "\n Zona de ubicacion de la entrada: " + entradas[i].getZona() +"\n precio de la entrada: "+ entradas[i].getPrecio() +"\n nombre comprador de la entrada: "+ entradas[i].getComprador() +"\n");
				
			}
			return true;
		}
		
		return false;
	}
	
	public boolean buscandoEntrada (int ID)
	{
		boolean existeEntrada= false;
		int i;
		
		for (i = 0; i<contEntradas; i++)
		{
			if (ID == entradas[i].getID())
			{
				existeEntrada = true;
			}
		}
		return existeEntrada;
	}
}	
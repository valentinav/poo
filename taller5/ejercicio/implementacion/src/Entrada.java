public abstract class Entrada
{
	private int OID;
	protected Persona comprador;
	protected int numZona;
	protected int valorEntrada = 0;
	
	public Entrada (int identificador, Persona comprador, int z)
	{
		OID = identificador;
		this.comprador = comprador;
		numZona = z;
	}
	
	public abstract void obtenerValorEntrada();
	
	public String getComprador()
	{
		return comprador.getNombre();
	}
	public int getID ()
	{
		return OID;
	}
	public int getZona ()
	{
		return numZona;
	}
	public int getPrecio()
	{
		return valorEntrada;
	}
}
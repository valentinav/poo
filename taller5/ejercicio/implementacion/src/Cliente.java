public class Cliente
{
	public static void main (String [] args)
	{
		Persona pas1, pas2, pas3, pas4;
		
		System.out.println ("\n -- Creando el Teatro -- ");
		Teatro Casae = new Teatro ();
		
		System.out.println ("\n -- Creando 4 Personas --");
		
			pas1 = new Persona ("estudiante", "pablo");
			pas2 = new Persona ("pensionado", "fede");
			pas3 = new Persona ("normal", "jonas");
			pas4 = new Persona ("abonado", "Dan");
		
		System.out.println ("\n --Vendiendo Entradas --");
		
			if (Casae.asignarEntrada(pas1, 1)) 
			{
				System.out.println ("nueva entrada generada");
			}
			else 
			{
				System.out.println ("No hay mas entradas disponibles");
			}
			if (Casae.asignarEntrada(pas2, 4)) 
			{
				System.out.println ("nueva entrada generada");
			}
			else 
			{
				System.out.println ("No hay mas entradas disponibles");
			}
			
			if (Casae.asignarEntrada(pas3, 2))
			{
				System.out.println ("nueva entrada generada");
			}
			else 
			{
				System.out.println ("No hay mas entradas disponibles");
			}
			
			if (Casae.asignarEntrada(pas4, 2))
			{
				System.out.println("nueva entrada generada");
			}
			else 
			{
				System.out.println ("No hay mas entradas disponibles");
			}

		System.out.println("\n -- listando entradas por zonas --");
		
			int i;
			for (i=0; i < 4; i++)
			{
				System.out.println("Zona"+ i);
			
				if (Casae.listarEntradas(i)==false)
				{
					System.out.println("Zona vacio\n");
				}
			}
		
		boolean indice = false;
		
		System.out.println ("\n -- buscando entradas --");
			if ((indice=Casae.buscarEntrada (111,1)) != false)
			{
				System.out.println("entrada hallada con exito");
			}
			else 
			{
				System.out.println("entrada no encontrada");
			}
			
			if ((indice=Casae.buscarEntrada(333,3)) != false)
			{
				System.out.println("entrada hallada con exito");
			}
			else 
			{
				System.out.println("entrada no encontrada");
			}
	}
}
public class Teatro
{
	private Zona zonas[];
	
	public Teatro ()
	{
		zonas = new Zona [4];
		zonas [0] = new Zona (1, "Principal", 20,111);
		zonas [1] = new Zona (2, "Palco", 10, 222);
		zonas [2] = new Zona (3, "Central", 40, 333);
		zonas [3] = new Zona (4, "Lateral", 10,444);
	}
	public boolean asignarEntrada (Persona usuario, int z)
	{
		return zonas[z-1].crearEntrada(usuario, z);
	}
	public boolean listarEntradas (int z)
	{
		return zonas[z].imprimirEntrada();
		
	}
	public boolean buscarEntrada (int ID,int z)
	{
		return zonas[z-1].buscandoEntrada(ID);
	}
}
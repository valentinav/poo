public class Persona
{
	private String Documento;
	private String Nombre;
	
	public Persona ()
	{
		Documento = null;
		Nombre = null;
	}
	
	public Persona(String Doc, String Nom)
	{
		Documento = Doc;
		Nombre = Nom;
	}
	
	public String getDocumento()
	{
		return Documento;
	}
	
	public String getNombre()
	{
		return Nombre;
	}
}  
public class EntradaAbonada extends Entrada
{
	public EntradaAbonada(int OID, Persona comprador, int numZona)
	{
		super(OID, comprador, numZona);
	}
	public void obtenerValorEntrada()
	{
		switch (numZona)
		{
			case 1:
				valorEntrada = 17500;
				break;
			
			case 2:
				valorEntrada = 40000;
				break;
			
			case 3:
				valorEntrada = 14000;
				break;
			
			case 4:
				valorEntrada = 10000;
				break;
		}
		
	}
}
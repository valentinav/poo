public class EntradaReducida extends Entrada
{	
	public EntradaReducida(int OID, Persona comprador, int numZona)
	{
		super(OID, comprador, numZona);
	}
	public void obtenerValorEntrada()
	{
		switch (numZona)
		{
			case 1:
				valorEntrada = 25000;
				valorEntrada -= (valorEntrada*0.15);
				break;
			
			case 2:
				valorEntrada = 70000;
				valorEntrada -= (valorEntrada*0.15);
				break;
			
			case 3:
				valorEntrada = 20000;
				valorEntrada -= (valorEntrada*0.15);
				break;
			
			case 4:
				valorEntrada = 15500;
				valorEntrada -= (valorEntrada*0.15);
				break;
		}
		
	}
}
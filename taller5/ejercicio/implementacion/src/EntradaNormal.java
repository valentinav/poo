public class EntradaNormal extends Entrada
{	
	public EntradaNormal(int OID, Persona comprador, int numZona)
	{
		super(OID, comprador, numZona);
	}
	
	public void obtenerValorEntrada()
	{
		switch (numZona)
		{
			case 1:
				valorEntrada = 25000;
				break;
			
			case 2:
				valorEntrada = 70000;
				break;
			
			case 3:
				valorEntrada = 20000;
				break;
			
			case 4:
				valorEntrada = 15500;
				break;
		}
		
	}
}
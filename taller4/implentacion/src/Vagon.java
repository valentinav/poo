public class Vagon
{
	private int SIZE = 12;
	private int numero;
	private int ContPasajeros;
	private Pasajero pasajeros [];
	
	public Vagon (int numero)
	{
		this.numero = numero;
		pasajeros = new Pasajero [SIZE];
		ContPasajeros = 0;
	}
	
	public boolean asignarPasajero (Pasajero pas)
	{
		boolean Rt = false;
		
		if (ContPasajeros <= SIZE)
		{
			pasajeros [ContPasajeros] = pas;
			Rt = true;
		}
		ContPasajeros ++;
		
		return Rt;
	}
	
	public boolean buscarPasajero (int cedula)
	{
		int i;
		
		for (i = 0; i<ContPasajeros; i++)
		{
			if (cedula == pasajeros [i].getCedula())
			{
				return true;
			}
		}
			return false;
	}
	
	public boolean imprimirPersona ()
	{	
		int i;
		
		if (ContPasajeros > 0 )
		{
			for (i = 0; i < ContPasajeros; i++)
			{
				
				System.out.println(pasajeros[i].getCedula()+ "\n" + pasajeros[i].getNombre() +"\n");
				
			}
			return true;
		}
		
		return false;
	}
}
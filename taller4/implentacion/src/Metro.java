public class Metro
{
	private int SIZE = 3;
	private Vagon vagones [];
	private String matricula;
	private boolean retornar = false;
	
	public Metro ()
	{
		vagones = new Vagon [SIZE];
		vagones [0] = new Vagon (1);
		vagones [1] = new Vagon (2);
		vagones [2] = new Vagon (3);
 		matricula = null;
	}
	
	public void setMatricula (String m)
	{
		matricula = m;
	}
	
	public int getSize()
	{
		return SIZE;
	}
	
	public boolean asignarVagon (Pasajero p, int numero)
	{
		return vagones[numero-1].asignarPasajero(p);
	
	}
	
	public boolean buscarPasajero (int cedula)
	{
		int i;
		
		for (i=0; i<SIZE; i++)
		{
		  retornar = vagones[i].buscarPasajero(cedula);
		}
		return retornar;
	}
	
	public boolean listarVagon (int numero)
	{
		return vagones[numero].imprimirPersona();
		
	}
} 
public class Pasajero
{
	private int cedula;
	private String Nombre;
	
	public Pasajero (int ID, String n)
	{
		cedula = ID;
		Nombre = n;
	}
	
	public int getCedula ()
	{
		return cedula;
	}
	
	public String getNombre()
	{
		return Nombre;
	}
}
public class Principal 
{
	public static void main (String args [])
	{
		Pasajero pas1, pas2, pas3, pas4;
		
		p("\n -- Creando el metro -- ");
		Metro Mio = new Metro ();
		Mio.setMatricula ("jj8428");
		
		p ("\n -- Creando 4 pasajeros --");
		
			pas1 = new Pasajero (10123456, "pablo");
			pas2 = new Pasajero (10246180, "fede");
			pas3 = new Pasajero (10121416, "jonas");
			pas4 = new Pasajero (10182022, "Dan");
			
		p ("\n --Registrando pasajeros --");
		
			if (Mio.asignarVagon(pas1, 1)) 
			{
				p ("Pasajero ced: " + pas1.getCedula()+" asignado al vagon "+1);
			}
			else 
			{
				p ("No hay cupos en el metro");
			}
			if (Mio.asignarVagon(pas2, 1)) 
			{
				p ("Pasajero ced: " + pas1.getCedula()+" asignado al vagon "+1);
			}
			else 
			{
				p ("No hay cupos en el metro");
			}
			
			if (Mio.asignarVagon(pas2, 2))
			{
				p("Pasajero ced: " + pas2.getCedula()+" asignado al vagon " +2);
			}
			else 
			{
				p ("No hay cupos en el metro");
			}
			
			if (Mio.asignarVagon(pas3, 1))
			{
				p ("Pasajero ced: " +pas3.getCedula()+ " asignado al vagon " +1);
			}
			else 
			{
				p ("No hay cupos en el metro");
			}
			
			if (Mio.asignarVagon(pas4, 3))
			{
				p("Pasajero ced: " +pas4.getCedula()+ " asignado al vagon " +3);
			}
			else 
			{
				p ("No hay cupos en el metro");
			}
			
			boolean indice = false;
			
		p ("\n -- buscando pasajeros --");
			if ((indice=Mio.buscarPasajero(10121418)) != false)
			{
				p("Pasajero ced: 10121418 encontrado en el vagon: "+indice);
			}
			else 
			{
				p("Pasajero ced: 10121418 no encontrado");
			}
			
			if ((indice=Mio.buscarPasajero(10182022)) != false)
			{
				p("Pasajero ced: 10182022 encontrado en el vagon: "+indice);
			}
			else 
			{
				p("Pasajero ced: 10182022 no encontrado");
			}
			
		p("\n -- listando vagones--");
		
		int i;
		for (i=0; i < Mio.getSize(); i++)
		{
			p("vagon"+ i);
			
			if (Mio.listarVagon(i)==false)
			{
				p("Vagon vacio");
			}
		}
	}
	
	public static void p(String s)
	{
		System.out.println(s);
	}
}
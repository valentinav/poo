public class Triangulo
{
	private int lado1;
	private int lado2;
	private int lado3;
	private double area;
	private double p;
	private double perimetro;
	
	public void iniciarLados (int L1, int L2, int L3)
	{
		if ( (L1>0) || (L2>0) || (L3>0) )
		{
			lado1 = L1;
			lado2 = L2;
			lado3 = L3;
		}
	}
	
	public double getPerimetro()
	{
		return perimetro;
	}
	
	public double getArea()
	{
		return area;
	}
	
	public void calcularPerimetro()
	{
		perimetro = (lado1+lado2+lado3);
	}
	public void calcularArea ()
	{
		p = (perimetro/2);
		area = Math.sqrt(p*(p-lado1)*(p-lado2)*(p-lado3));
	}
}
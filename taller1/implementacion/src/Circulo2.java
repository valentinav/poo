public class Circulo2{
	
	private int radio;
	private double area;
	private double perimetro;
	
	public void inicializar (int RADIO) {
	
	radio = RADIO; 
	}
	
	public int getRadio () {
	return radio;
	}
	
	public double getArea () {
	return area;
	}
	
	public double getPerimetro() {
	return perimetro;
	}
	
	public void calcularArea () {
	area = Math.PI*radio*radio;
	}
	
	public void calcularPerimetro () {
	perimetro = 2*Math.PI*radio;
	}
	
}
public class Usuario
{
	
	public static void main(String[] args){
		
		Triangulo t1= new Triangulo ();
		t1.iniciarLados (3,4,5);
		t1.calcularPerimetro();
		t1.calcularArea();
		
		System.out.println("Datos del triangulo:");
		System.out.println("El perimetro es:" + t1.getPerimetro());
		System.out.println("El area es:" + t1.getArea());
	}
}
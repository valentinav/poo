import java.util.Random;

public class JuegoAdivina
{
	protected int Numero;
	Random rnd = new Random();
		
	public JuegoAdivina()
	{
		Numero = 0;
	}
	
	public void generarNumero()
	{
	}
	public int compararNumero(int num) throws FueraRangoException
	{
		if ((num > 10)||(num < 0)) throw (new FueraRangoException());
		int retorno = 0;
		
		if(num == Numero)
		{
			retorno = 0;
		}
		if (num < Numero)
		{
			retorno = 1;
		}
		if (num > Numero)
		{
			retorno = -1;
		}
		
		return retorno;
	}
	public int getNumero()
	{
		return Numero;
	}
	
}
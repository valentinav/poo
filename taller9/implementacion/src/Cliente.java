import java.util.Scanner;

public class Cliente
{
	public static void main(String[] args)
	{
		int num = 0;
		int Contador= 0;
		int Resultado=0;
		
		JuegoAdivinaEntero J = new JuegoAdivinaEntero();
		J.generarNumero();
		
		do
		{
			Scanner teclado = new Scanner (System.in);
		
			num = teclado.nextInt();
			Contador ++;
		
			try
			{
				Resultado = J.compararNumero (num);
				System.out.println(J.getNumero());
				if(Resultado == 0)
				{
					System.out.println ("Ganaste!!");
					Contador = 8;
				}
				if (Resultado == 1)
				{
					System.out.println("El numero es mayor");
				}
				if (Resultado == -1)
				{
					System.out.println("El numero es menor");
				}
			}
			catch (FueraRangoException e)
			{
				System.err.println(e.getMessage());
				e.printStackTrace();
				break;
			}
				
		} while((Contador < 5));
	}
}
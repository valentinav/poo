public class FueraRangoException extends Exception
{
	public FueraRangoException()
	{
		super("El numero debe encontrarse entre 0 y 10");
	}
}
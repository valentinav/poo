public class Cliente
{
 public static void main (String[] args)
 {
	Empleado e1 = new Empleado();
	
	System.out.println ("Nombre: " + e1.getNombre());
	System.out.println ("Apellido:" + e1.getApellido());
	System.out.println ("Edad:" + e1.getEdad());
	System.out.println ("salario:" + e1.calcularSalario(50));
	
	Empleado e2 = new Empleado ("andres", "ramirez", 24);
	
	System.out.println ("Nombre: " + e2.getNombre());
	System.out.println ("Apellido:" + e2.getApellido());
	System.out.println ("Edad:" + e2.getEdad());
	System.out.println ("El salario es:" + e2.calcularSalario(10));
 }
 
}
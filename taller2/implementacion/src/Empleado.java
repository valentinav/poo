public class Empleado{
 
  private String nombre;
  private String apellido;
  private int edad;
  private int salario;
  private int horas;
  
	public Empleado ()
	{
	nombre = "valentina";
	apellido = "vera";
	edad = 18;
	}
	
	public Empleado (String n, String a, int e){
	
		nombre = n;
		apellido = a;
		edad = e;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public int getEdad() {
		return edad;
	}
	
	
	
	public int calcularSalario (int horas) {
		
		if (horas <= 40) {
		salario = 12000*horas;
		}
		
		if (horas > 40) {
		salario = (12000*40) + ((horas-40)*16000);
		}
		
		return salario;
	}
	
}
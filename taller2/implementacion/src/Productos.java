public class Productos
{

  private String nombreProducto;
  private int codigo;
  private String categoria;
  private double costo;
  private double IVA;
  private int cantidad;
	
	public Productos(String np, int cod, String cat,int cantid, double precio)
	{
		nombreProducto = np;
		codigo = cod;
		categoria = cat;
		cantidad = cantid;
		costo = precio;
	}
	
	public void calculandoIVA ()
	{
		if (costo >= 1000)
		{
			IVA = (costo*(0.03));
		}
		
	} 
	
	public double valorTotal ()
	{
		if (cantidad > 0 )
		{
			costo = (costo+IVA)*cantidad;
		}
		else
		{
			costo = costo+IVA;
		}
		return costo;
	}
	
	public String getNombre()
	{
		return nombreProducto;
	}
	
	public int getCodigo()
	{
		return codigo;
	}
	
	public String getCategoria()
	{
		return categoria;
	}
	
	public double getCosto()
	{
		return costo;
	}
	
	public double getIVA()
	{
		return IVA;
	}
	public int getCantidad()
	{
		return cantidad;
	}
}
public class Cajero
{
	public static void main (String[] args)
	{
		Productos pr1 = new Productos ("leche", 3020, "lacteos", 4, 1200);
		pr1.calculandoIVA();
		
		System.out.println ("informacion del producto \n");
		
		System.out.println ("producto: " + pr1.getNombre());
		System.out.println ("codigo: " + pr1.getCodigo());
		System.out.println ("categoria: " + pr1.getCategoria());
		System.out.println ("costo: " + pr1.getCosto());
		System.out.println ("IVA: " + pr1.getIVA());
		System.out.println ("\n");
		
		System.out.println ("valor segun el IVA y la cantidad: \n");
		System.out.println ("producto: " + pr1.getCodigo());
		System.out.println ("cantidad: "+ pr1.getCantidad());
		System.out.println ("costo: " + pr1.valorTotal());
		System.out.println ("\n\n");
		
		Productos pr2 = new Productos ("tomate", 2023, "verduras", 3, 400);
		pr2.calculandoIVA();
		
		System.out.println ("informacion del producto\n");
		
		System.out.println ("producto: " + pr2.getNombre());
		System.out.println ("codigo: " + pr2.getCodigo());
		System.out.println ("categoria: " + pr2.getCategoria());
		System.out.println ("costo: " + pr2.getCosto());
		System.out.println ("IVA: " + pr2.getIVA());
		System.out.println ("\n");
		
		System.out.println ("valor segun el IVA y la cantidad: \n");
		System.out.println ("producto: " + pr2.getCodigo());
		System.out.println ("cantidad: "+ pr2.getCantidad());
		System.out.println ("costo: " + pr2.valorTotal());
		System.out.println ("\n\n");
		
		Productos pr3 = new Productos ("mantequilla", 1263, "lacteos", 1, 2000);
		pr3.calculandoIVA();
		
		System.out.println ("informacion del producto\n");
		
		System.out.println ("producto: " + pr3.getNombre());
		System.out.println ("codigo: " + pr3.getCodigo());
		System.out.println ("categoria: " + pr3.getCategoria());
		System.out.println ("costo: " + pr3.getCosto());
		System.out.println ("IVA: " + pr3.getIVA());
		System.out.println ("\n");
		
		System.out.println ("valor segun el IVA y la cantidad: \n");
		System.out.println ("producto: " + pr3.getCodigo());
		System.out.println ("cantidad: "+ pr3.getCantidad());
		System.out.println ("costo: " + pr3.valorTotal());
		System.out.println ("\n\n");
		
		Productos pr4 = new Productos ("huevo", 2225, "proteina", 5, 200);
		pr4.calculandoIVA();
		
		System.out.println ("informacion del producto\n");
		
		System.out.println ("producto: " + pr4.getNombre());
		System.out.println ("codigo: " + pr4.getCodigo());
		System.out.println ("categoria: " + pr4.getCategoria());
		System.out.println ("costo: " + pr4.getCosto());
		System.out.println ("IVA: " + pr4.getIVA());
		System.out.println ("\n");
		
		System.out.println ("valor segun el IVA y la cantidad: \n");
		System.out.println ("producto: " + pr4.getCodigo());
		System.out.println ("cantidad: "+ pr4.getCantidad());
		System.out.println ("costo: " + pr4.valorTotal());
		System.out.println ("\n\n");
		
		Productos pr5 = new Productos ("lentejas", 0325, "legumbres", 1, 800);
		pr5.calculandoIVA();
		
		System.out.println ("informacion del producto\n");
		
		System.out.println ("producto: " + pr5.getNombre());
		System.out.println ("codigo: " + pr5.getCodigo());
		System.out.println ("categoria: " + pr5.getCategoria());
		System.out.println ("costo: " + pr5.getCosto());
		System.out.println ("IVA: " + pr5.getIVA());
		System.out.println ("\n");
		
		System.out.println ("valor segun el IVA y la cantidad: \n");
		System.out.println ("producto: " + pr5.getCodigo());
		System.out.println ("cantidad: "+ pr5.getCantidad());
		System.out.println ("costo: " + pr5.valorTotal());
		System.out.println ("\n\n");

	}

}
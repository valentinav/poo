public class Main
{
	public static void main (String [] args)
	{
		AguaPubenza instancia = new AguaPubenza();
		
		ClienteProvincia cli1 = new ClienteProvincia (98393281, "Juan Perez", 15);
		ClienteProvincia cli2 = new ClienteProvincia (98393282, "Maria Cardenas", 18);
		ClienteCapital cli3 = new ClienteCapital (98393284, "Fernando Sotelo", 25, "Cll 12 No. 12-12");
		
		instancia.agregarClientes(cli1);
		instancia.agregarClientes(cli2);
		instancia.agregarClientes(cli3);
		
		System.out.println ("Listado de clientes con facturas:\n");
		instancia.listarClientes();
		
		System.out.println ("\n Buscando un cliente \n");
		
		Cliente cl = instancia.buscarClientes(98393282);
		if (cl == null)
		{
			System.out.println("Cliente no existe");
		}
		else 
		{
			instancia.mostrar();
		}
	}
}
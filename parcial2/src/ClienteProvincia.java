public class ClienteProvincia extends Cliente
{	
	public ClienteProvincia (int Cedula, String Nombre, int ConsumoEnLitros)
	{
		super(Cedula, Nombre, ConsumoEnLitros);
	}
	
	public double generarFactura ()
	{
		FACTURA = (ConsumoEnLitros*ValorLitro)+((ConsumoEnLitros*2580)*0.1);
		return FACTURA;
	}
}
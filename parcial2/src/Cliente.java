public class Cliente
{
	protected int TARIFA_SANEAMIENTO;
	protected int TARIFA_RESIDUALES;
	protected int ValorLitro;
	protected int Cedula;
	protected String Nombre;
	protected int ConsumoEnLitros;
	protected double FACTURA;
	
	public Cliente(int Cedula, String Nombre, int ConsumoEnLitros)
	{
		TARIFA_SANEAMIENTO = 3500;
		TARIFA_RESIDUALES = 4600;
		ValorLitro = 2580;
		this.Cedula = Cedula;
		this.Nombre = Nombre;
		this.ConsumoEnLitros = ConsumoEnLitros;
	}
	
	protected double generarFactura ()
	{
		FACTURA =((ConsumoEnLitros*ValorLitro)+ TARIFA_SANEAMIENTO + TARIFA_RESIDUALES);
		return FACTURA;
	} 
	public String toString()
	{
		return "Cedula: " + Cedula + " Nombre: " + Nombre + " Factura: " + generarFactura();
	}
	public int getCedula ()
	{
		return Cedula;
	}
	public String getNombre ()
	{
		return Nombre;
	}
	
	
}
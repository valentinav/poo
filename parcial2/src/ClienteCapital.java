public class ClienteCapital extends Cliente
{
	private String Direccion;
	
	public ClienteCapital (int Cedula, String Nombre, int ConsumoEnLitros, String Direccion)
	{
		super(Cedula, Nombre, ConsumoEnLitros);
		this.Direccion = Direccion;
	}
	public double generarFactura ()
	{
		FACTURA = super.generarFactura()-(super.generarFactura()*0.25);
		return FACTURA;
	}
	
	public String getDireccion()
	{
		return Direccion;
	}
	public String toString ()
	{
		return "Cedula: " + getCedula() + " Nombre: " + getNombre() + " Factura: " + generarFactura() + " Direccion: " + Direccion;
	}
}